<!DOCTYPE html>
<html>
<head>
    <title>Affichage des données</title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        th {
            background-color: #f2f2f2;
        }

        tr:nth-child(even) {
            background-color: #f9f9f9;
        }

        input[type="submit"] {
            background-color: #ff0000;
            color: white;
            border: none;
            padding: 5px 10px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            cursor: pointer;
        }

        input[type="submit"]:hover {
            background-color: #cc0000;
        }
        button {
            background-color: #4CAF50; 
            color: white; 
            padding: 10px 20px; 
            border: none; 
            border-radius: 5px; 
            text-align: center; 
            text-decoration: none; 
            display: inline-block;
            font-size: 16px; 
            margin-top: 10px; 
        }

        button:hover {
            background-color: #45a049; 
        }
    </style>
</head>
<body>
    <h2>Liste of users</h2>
    <a href="index.html"><button>New</button></a>
    <table border="1">
        <tr>
            <th>Name</th>
            <th>Surname</th>
            <th>Phone</th>
            <th>E-mail</th>
            <th>Action</th>

        </tr>
        <?php
        include "cnx.php"; 

        $query = "SELECT COUNT(*) as total FROM user"; 
    $result = mysqli_query($link, $query);
    $row = mysqli_fetch_assoc($result);

    if ($row['total'] == 0) {
        header("Location: index.html"); 
        exit();
    }

        $query = "SELECT * FROM user"; 
        $result = mysqli_query($link, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            echo "<tr>";
            echo "<td>" . $row['nom'] . "</td>";
            echo "<td>" . $row['prenom'] . "</td>";
            echo "<td>" . $row['tel'] . "</td>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>
                    <form method='post' action='suppresion.php'>
                        <input type='hidden' name='id' value='" . $row['id'] . "'>
                        <input type='submit' name='delete' value='Supprimer'>
                    </form>
                  </td>";
            echo "</tr>";

        }
        ?>
    </table>
</body>
</html>
